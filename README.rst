###########
Synkrominos
###########

**Synkrominos provides visual support (card game) to facilitate meetings.**

You can buy the cards AND the project is distributed under creative commons license.
Here is the code repository. It contains "sources" of the project: graphic files, translations, ...
Contributions are welcome!

* http://synkrominos.com
* contact@synkrominos.com


************
Installation
************

Here are some notes to setup the project in order to develop or build the sources.
At this stage, they are **notes** about environment of original developer...
Let's get in touch if you have any question, so that we can improve the setup process!

Third-party software installed and used to build the sources:

* Krita
* Inkscape
* Scribus version > 1.5.1 so that we have "command line" feature.
   Notice that generate.py currently refers to ``scribus-1.5.5-linux-x86_64.AppImage``, hardcoded.
* Python 3, pipenv (then run ``pipenv install``)

Fonts:

* Quicksand


***********
i18n & l10n
***********

.. code:: sh

   # Examples using "fr" language.

   # If you want to work on a new language...
   pybabel init --input-file=locale/messages.pot --output-dir=locale --locale=fr

   # When sources have changed, extract translation strings from sources...
   # ... then update .po files
   pybabel extract --mapping=babel.ini --output=locale/messages.pot .
   pybabel update --input-file=locale/messages.pot --output-dir=locale --locale=fr

   # Once you have translated strings in .po files, compile them as .mo
   # Note: this part is also automatically done in generate.py
   mkdir -p var/locale/fr/LC_MESSAGES && pybabel compile --input-file=locale/fr/LC_MESSAGES/messages.po --directory=var/locale --locale=fr


************************
Build files for printers
************************

* Get .mo translations (see instructions above)
* Use ``python generate.py`` to generate files.
  See ``python generate.py --help`` for options.

Examples:

.. code:: sh

   # Generate all files for "fr" language, with version name "test"
   python generate.py --locale=fr --bundle=stages --version="test"
   
   # Generate only "front" part of "welcome" card
   python generate.py --locale=fr --bundle=stages --version="test" --template='*front*' --id=welcome

Generated files are put in ``var`` folder, which is automatically created in working directory.


**********
References
**********

* Website: http://synkrominos.com
* Wiki: https://harigorria.frama.wiki/synkrominos
* Code repository: https://framagit.org/harigorria/synkrominos
* Contact: contact@synkrominos.com
