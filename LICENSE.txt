Synkrominos, CC-BY-SA Benoît Bryon, Sylvain Lebosquain and Delphine Saint Quentin.


###########
Synkrominos
###########

Synkrominos is a collective creation. Original authors are copyright holders:

* Benoît Bryon <benoit@marmelune.net>
* Sylvain Lebosquain <sylvain.lebosquain@free.fr>
* Delphine Saint Quentin <delphinesq@yahoo.fr>

Synkrominos is licensed under a
Creative Commons Attribution-ShareAlike 4.0 License
https://creativecommons.org/licenses/by-sa/4.0/

Contribution details for version 1.0:

* Concept and texts: Benoît Bryon, Sylvain Lebosquain, Delphine Saint Quentin
* Illustrations: Benoît Bryon
* Cards design: Benoît Bryon
* Programming: Benoît Bryon


#########
Libraries
#########

This project uses third-party material:

Fonts
=====

Quicksand (c) by Andrew Paglinawan
SIL Open Font License (http://scripts.sil.org/OFL)

Icons
=====

Font Awesome Free (c) Font Awesome
CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
Details at https://fontawesome.com/license/free
