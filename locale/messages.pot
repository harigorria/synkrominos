# Translations template for PROJECT.
# Copyright (C) 2020 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2020-06-28 23:20+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: bundles/stages.json:7
msgid "Welcome"
msgstr ""

#: bundles/stages.json:8
msgid "<strong>Be welcome!</strong><br />Meet up<br />casual discussions."
msgstr ""

#: bundles/stages.json:15
msgid "Circle"
msgstr ""

#: bundles/stages.json:16
msgid "Take our place :)"
msgstr ""

#: bundles/stages.json:23
msgid "Intention"
msgstr ""

#: bundles/stages.json:24
msgid ""
"<strong>What do we meet for?</strong><br />Which invitation<br />we "
"honour?"
msgstr ""

#: bundles/stages.json:31
msgid "Weather"
msgstr ""

#: bundles/stages.json:32
msgid ""
"<strong>How are you?</strong><br />Status and availability<br />authentic"
" and sincere<br />don't detail your life"
msgstr ""

#: bundles/stages.json:39
msgid "Framework"
msgstr ""

#: bundles/stages.json:40
msgid ""
"<strong>What do you need to</strong><br /><strong>fully express "
"yourselves?</strong><br />Time, environment,<br />relationships..."
msgstr ""

#: bundles/stages.json:47
msgid "Enrolment"
msgstr ""

#: bundles/stages.json:48
msgid "<strong>How to dispatch roles?</strong><br />Travel, memory, expression..."
msgstr ""

#: bundles/stages.json:55
msgid "Expectations"
msgstr ""

#: bundles/stages.json:56
msgid ""
"<strong>What would make you happy</strong><br /><strong>at the end of "
"this meeting?</strong><br /><br />"
msgstr ""

#: bundles/stages.json:63
msgid "Order of the day"
msgstr ""

#: bundles/stages.json:64
msgid ""
"<strong>What time for what activity</strong><br />in this meeting?<br "
"/>What are the priorities?<br />"
msgstr ""

#: bundles/stages.json:71
msgid "Action"
msgstr ""

#: bundles/stages.json:72
msgid "<strong>Time to get it done!</strong><br />Let's go :)<br /><br />"
msgstr ""

#: bundles/stages.json:79
msgid "Free time"
msgstr ""

#: bundles/stages.json:80
msgid ""
"Have a break?<br /><strong>Time for something else!</strong><br /><br "
"/>Come back in ... minutes!"
msgstr ""

#: bundles/stages.json:87
msgid "Small steps"
msgstr ""

#: bundles/stages.json:88
msgid ""
"<strong>What commitment(s)</strong><br /><strong>for "
"yourself?</strong><br />Specific, Measureable, Achievable<br />Realistic,"
" Time-bound."
msgstr ""

#: bundles/stages.json:95
msgid "Retrospective"
msgstr ""

#: bundles/stages.json:96
msgid ""
"Look back at the meeting:<br /><strong>learnings, regulation,</strong><br"
" />strenghts, enhancements,<br />gems..."
msgstr ""

#: bundles/stages.json:103
msgid "Celebration"
msgstr ""

#: bundles/stages.json:104
msgid ""
"Yeah! <strong>BRAVO!</strong><br /><strong>You are wonderful!</strong><br"
" />Greetings!<br />"
msgstr ""

#: bundles/stages.json:110
msgid "memory"
msgstr ""

#: bundles/stages.json:111
msgid "what will be useful later<br />has been collected and shared"
msgstr ""

#: bundles/stages.json:112
msgid "informations, resources,<br />followed path,<br />decisions, small steps"
msgstr ""

#: bundles/stages.json:113
msgid "notes, photos,<br />collecting productions,<br />sharing documents"
msgstr ""

#: bundles/stages.json:114
msgid "What have to persist<br />beyond this journey?"
msgstr ""

#: bundles/stages.json:119
msgid "journey"
msgstr ""

#: bundles/stages.json:120
msgid "followed path<br />was consistent with<br />expectations and context"
msgstr ""

#: bundles/stages.json:121
msgid "progress pace,<br />breaks and digressions,<br />available time left"
msgstr ""

#: bundles/stages.json:122
msgid "synkrominos,<br />journey path,<br />clock and timers"
msgstr ""

#: bundles/stages.json:123
msgid "How to organize<br />this collective space-time?"
msgstr ""

#: bundles/stages.json:128
msgid "expression"
msgstr ""

#: bundles/stages.json:129
msgid "everyone contributed<br />in good conditions"
msgstr ""

#: bundles/stages.json:130
msgid ""
"speaking, silence, tone, noise,<br />non verbal, simultaneous "
"discussions,<br />singularities of persons"
msgstr ""

#: bundles/stages.json:131
msgid ""
"speaking turns and durations,<br />framework, say I,<br />speaking to the"
" center, gestures"
msgstr ""

#: bundles/stages.json:132
msgid "How to express wholeheartedly<br />and be listened?"
msgstr ""

#: bundles/stages.json:137
msgid "gratitude"
msgstr ""

#: bundles/stages.json:138
msgid ""
"<strong>Presence of several persons in the same space-time is precious! "
"How to make such meetings joyful, serene and efficient?</strong><br "
"/>What about revealing the steps for a group to synchronize? Place "
"<strong>SYNKROMINOS</strong> in the centre, arrange an itinerary, "
"distribute roles...<strong>Make the rules that fit both individuals and "
"collectives :)</strong><br />Have a nice journey!"
msgstr ""

#: bundles/stages.json:139
msgid "Benoît Bryon<br />Sylvain Lebosquain<br />Delphine Saint Quentin"
msgstr ""

#: bundles/stages.json:140
msgid "with 100+ contributors"
msgstr ""

#: bundles/stages.json:149
msgid ""
"Synkrominos<br />    activity under CAPE agreement<br />    SCIC "
"Interstices Sud Aquitaine<br />    3 rue Hélène Boucher<br />    40220 "
"Tarnos - FRANCE"
msgstr ""

#: bundles/stages.json:150
msgid "with 100+ contributors :)"
msgstr ""

#: bundles/stages.json:151
msgid "CC-BY Font Awesome"
msgstr ""

#: bundles/stages.json:152
msgid "moo.com - United Kingdom"
msgstr ""

#: bundles/stages.json:153
msgid "Warning! Small parts,<br />chocking hazard!"
msgstr ""

#: layouts/moo 55x84/gratitude_front.sla:302
msgid "icons CC-BY Font Awesome"
msgstr ""

#: layouts/moo 55x84/legal_notices.svg:2966
msgid "Version / Batch:"
msgstr ""

#: layouts/moo 55x84/legal_notices.svg:2973
msgid "Manufacturing:"
msgstr ""

#: layouts/moo 55x84/legal_notices.svg:2986
msgid "Edition:"
msgstr ""

#: layouts/moo 55x84/legal_notices.svg:4073
msgid "Icons:"
msgstr ""

#: layouts/moo 55x84/medal.svg:775
msgid "contribution"
msgstr ""

#: layouts/moo 55x84/role_back.sla:106 layouts/moo 55x84/role_front.sla:110
msgid "Roles"
msgstr ""

#: layouts/moo 55x84/role_back.sla:116
msgid "I take care of:"
msgstr ""

#: layouts/moo 55x84/role_back.sla:126
msgid "My means of action:"
msgstr ""

#: layouts/moo 55x84/role_back.sla:131
msgid "I pay attention to:"
msgstr ""

