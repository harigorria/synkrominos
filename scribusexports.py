"""Produces PDF or PNG for one or multiple SLA files.

Usage samples:

* scribus --no-gui --no-splash -py scribusexports.py -- file_to_export.sla
* scribus --no-gui --no-splash -py scribusexports.py file1.sla file2.sla --
* scribus --no-gui --no-splash -py scribusexports.py --output-name=export.pdf file.sla --
* scribus --no-gui --no-splash -py scribusexports.py --output-name=export.png file.sla --

Script inspired by "to-pdf.py", Ale Rimoldi, MIT license.

"""

import argparse
import os
import scribus


def to_pdf(output_name):
    pdf = scribus.PDFfile()
    pdf.file = output_name
    pdf.save()


def to_png(output_name):
    image = ImageExport()
    image.type = 'PNG'
    image.dpi = 300
    image.quality = 100
    image.scale = 100
    image.name = output_name
    image.save()


def export_doc(output_name_template):
    filename = os.path.splitext(scribus.getDocName())[0]
    output_name = output_name_template.format(name=filename)
    if output_name.endswith('.pdf'):
        to_pdf(output_name)
    elif output_name.endswith('.png'):
        to_png(output_name)


def export_docs(names=[], output_name_template='{name}.pdf'):
    if scribus.haveDoc():
        export_doc(output_name_template)
        scribus.closeDoc()
    else:
        doc_list = args.names
        for doc_path in doc_list:
            scribus.openDoc(doc_path)
            export_doc(output_name_template)
            scribus.closeDoc()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate PDF from current doc Xor doc list.')
    parser.add_argument('names', metavar='NAME', type=str, nargs='*', help='Files to process.', default=[])
    parser.add_argument('--output-name', type=str, default='{name}.pdf', help='Template for output file name.', dest="output_name")
    args = parser.parse_args()
    export_docs(args.names, args.output_name)
