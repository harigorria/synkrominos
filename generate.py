#!/usr/bin/python
# coding=utf-8

from __future__ import print_function
import argparse
from collections import OrderedDict
import fnmatch
import json
import re
import shlex
import subprocess
from xml.etree import ElementTree

from babel.support import Translations
import jinja2
import pathlib


SOURCECODE_DIR = pathlib.Path(__file__).parent.resolve()
WORKING_DIR = SOURCECODE_DIR
BUNDLES_DIR = WORKING_DIR / 'bundles'
IMAGE_DIR = WORKING_DIR / 'img'
LAYOUTS_DIR = WORKING_DIR / 'layouts'
LOCALE_DIR = WORKING_DIR / 'locale'
BUILD_DIR = WORKING_DIR / 'var'

BUILD_VERSION = ':)'


def compile_locale(locale):
    """Make sure .mo file is available for given ``locale``."""
    output_dir = pathlib.Path("var/locale/{locale}/LC_MESSAGES".format(locale=locale))
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    mo_file = output_dir / "messages.mo"
    if not mo_file.exists():
        command = "pybabel compile --input-file=locale/{locale}/LC_MESSAGES/messages.po --directory=var/locale --locale={locale}".format(
            locale=shlex.quote(locale),
        )
        print(command)
        subprocess.call(command, shell=True)


def html2inkscape(value):
    """Return HTML input as XML to be inserted in Inkscape's <TSPAN> element.
    
    Given Inkscape's SVG <TSPAN>[somecontent]</TSPAN>,
    where [somecontent] is very basic inline HTML code,
    convert [somecontent] to Inkscape's SVG (i.e. XML),
    optionnally including some "</tspan><tspan>".
    
    Only **very basic** HTML is supported:
    
    * unformatted plain text
    * STRONG tag
    * BR
    
    >>> print(html2inkscape(u"Hello world!"))
    Hello world!
    
    >>> print(html2inkscape(u"<strong>Important</strong>"))
    <tspan style="font-weight:bold;">Important</tspan>
    
    >>> print(html2inkscape(u"<STRONG>Important</STRONG>"))
    <tspan style="font-weight:bold;">Important</tspan>
    
    >>> print(html2inkscape(u""))
    <BLANKLINE>
    
    >>> print(html2inkscape(u"Regular and <strong>important</strong> content."))
    Regular and <tspan style="font-weight:bold;">important</tspan> content.

    >>> print(html2inkscape(u"Multiline<br /><strong>sample</strong>"))
    Multiline</tspan><tspan sodipodi:role=\"line\"><tspan style="font-weight:bold;">sample</tspan>
    
    >>> print(html2inkscape(u"Multiline inside <strong>bold<br />text</strong> !"))
    Multiline inside <tspan style="font-weight:bold;">bold</tspan></tspan><tspan sodipodi:role=\"line\"><tspan style="font-weight:bold;">text</tspan> !
    """
    def text2inkscape(value):
        return value

    def strongtext2inkscape(value):
        return u'<tspan style="font-weight:bold;">{}</tspan>'.format(value) if value else u''
    
    def linebreak2inkscape():
        return u'</tspan><tspan sodipodi:role="line">'

    def element2inkscape_elements(element):
        """Return list of """
        if element.tag.lower() == u'p':
            yield text2inkscape(element.text)
        elif element.tag.lower() == u'strong':
            yield strongtext2inkscape(element.text)
        elif element.tag.lower() == u'br':
            yield linebreak2inkscape()
        for child in element:
            for item in element2inkscape_elements(child):
                yield item
            if child.tail:
                if element.tag.lower() == u'p':
                    yield text2inkscape(child.tail)
                elif element.tag.lower() == u'strong':
                    yield strongtext2inkscape(child.tail)

    root = ElementTree.fromstring('<p>{}</p>'.format(value))
    output = u''.join([part for part in element2inkscape_elements(root) if part])
    return output


def html2scribus(value, strong_font):
    """Return HTML input as XML to be inserted in Scribus <ITEXT> element.
    
    Only **very basic** HTML is supported:
    
    * unformatted plain text
    * STRONG tag
    * BR

    >>> strong_font = u"Liberation Sans Bold"
    
    >>> print(html2scribus(u"Hello world!", strong_font))
    <ITEXT CH="Hello world!"/>
    
    >>> print(html2scribus(u"<strong>Important</strong>", strong_font))
    <ITEXT CH="Important" FONT="Liberation Sans Bold"/>
    
    >>> print(html2scribus(u"<STRONG>Important</STRONG>", strong_font))
    <ITEXT CH="Important" FONT="Liberation Sans Bold"/>
    
    >>> print(html2scribus(u"", strong_font))
    <BLANKLINE>
    
    >>> print(html2scribus(u"Regular and <strong>important</strong> content.", strong_font))
    <ITEXT CH="Regular and "/>
    <ITEXT CH="important" FONT="Liberation Sans Bold"/>
    <ITEXT CH=" content."/>

    >>> print(html2scribus(u"Multiline<br /><strong>sample</strong>", strong_font))
    <ITEXT CH="Multiline"/>
    <para/>
    <ITEXT CH="sample" FONT="Liberation Sans Bold"/>
    
    >>> print(html2scribus(u"Multiline inside <strong>bold<br />text</strong> !", strong_font))
    <ITEXT CH="Multiline inside "/>
    <ITEXT CH="bold" FONT="Liberation Sans Bold"/>
    <para/>
    <ITEXT CH="text" FONT="Liberation Sans Bold"/>
    <ITEXT CH=" !"/>

    """
    def text2scribus(value):
        """Return simple <ITEXT />."""
        return u'<ITEXT CH="{}"/>'.format(value) if value else u''

    def strongtext2scribus(value, strong_font):
        """Return <ITEXT /> with FONT attribute."""
        return u'<ITEXT CH="{value}" FONT="{font}"/>'.format(value=value, font=strong_font) if value else u''
    
    def linebreak2scribus():
        """Return <para/>."""
        return u'<para/>'

    def element2scribus_elements(element, strong_font):
        """Yield elements converted to Scribus."""
        if element.tag.lower() == u'p':
            yield text2scribus(element.text)
        elif element.tag.lower() == u'strong':
            yield strongtext2scribus(element.text, strong_font)
        elif element.tag.lower() == u'br':
            yield linebreak2scribus()
        for child in element:
            for item in element2scribus_elements(child, strong_font):
                yield item
            if child.tail:
                if element.tag.lower() == u'p':
                    yield text2scribus(child.tail)
                elif element.tag.lower() == u'strong':
                    yield strongtext2scribus(child.tail, strong_font)

    root = ElementTree.fromstring('<p>{}</p>'.format(value))
    output = u'\n'.join([part for part in element2scribus_elements(root, strong_font) if part])
    return output


def html2innerscribus(value, strong_font):
    """Convert HTML for use **within** CH attribute of ITEXT element."""
    value = html2scribus(value, strong_font)
    value = re.sub(r'(\s<para/>)+$', '', value)  # Trim trailing linebreaks.
    value = value[len('<ITEXT CH="'):-len('"/>')]  # Value is to be inserted inside CH attribute.
    return value


def jinja2_environment(locale):
    """Setup and return Jinja2 environment."""
    env = jinja2.Environment(
        loader=jinja2.FileSystemLoader('.'),
        extensions=['jinja2.ext.i18n'],
    )
    env.filters['html2inkscape'] = html2inkscape
    env.filters['html2scribus'] = html2innerscribus
    env.globals = {
        'locale': locale,
    }
    # i18n
    locale_dir = "var/locale"
    msgdomain = "messages"
    locales = [locale]  # List of locales in order of priority.
    translations = Translations.load(locale_dir, locales)
    env.install_gettext_translations(translations)
    return env


def load_bundle(bundle, locale):
    """Return list of items in bundle."""
    jinja2_env = jinja2_environment(locale=locale)
    bundle_filename = pathlib.Path("bundles", "{}.json".format(bundle))
    template = jinja2_env.get_template(str(bundle_filename))
    context_data = {}
    items_json = template.render(**context_data)
    items = OrderedDict()
    for item in json.loads(items_json):
        # Initialize paths
        item['illustration'] = "{build_dir}/img/{id}.png".format(
            build_dir=BUILD_DIR,
            id=item['id'],
        )
        item['icon'] = "{build_dir}/img/icons/{id}.pdf".format(
            build_dir=BUILD_DIR,
            id=item['id'],
        )

        items[item["id"]] = item
    return items


def kra2png(code):
    """Generate PNG files from Krita sources."""
    input_filename = IMAGE_DIR / "{code}.kra".format(code=code)
    if not input_filename.exists():
    	return None
    output_dir = BUILD_DIR / "img"
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    output_filename = output_dir / "{code}.png".format(code=code)
    command = "krita {input} --export --export-filename {output}"
    command = command.format(
        input=shlex.quote(str(input_filename.resolve())),
        output=shlex.quote(str(output_filename.resolve())),
    )
    print(command)
    subprocess.call(command, shell=True)
    return output_filename


def icon2pdf(code):
    """Generate PDF file for icon."""
    input_path = IMAGE_DIR / "icons" / "{}.svg".format(code)
    output_path = BUILD_DIR / "img" / "icons" / "{}.pdf".format(code)
    try:
        output_path.parent.mkdir(parents=True)
    except OSError:
        pass
    return inkscape2pdf(input_path, output_path)


def render_layout_template(item, items, layout_name, template_name, locale, defaults={}):
    """Generate layout (.svg, .sla) file from template and card."""
    jinja2_env = jinja2_environment(locale=locale)
    template_dir = pathlib.Path("layouts", layout_name)
    template_path = template_dir / template_name
    template = jinja2_env.get_template(str(template_path))
    output_ext = template_path.suffix
    output_dir = pathlib.Path("var", "layouts", layout_name)
    output_name = "{code} - {locale} - {template}{ext}".format(
        code=item['id'],
        ext=output_ext,
        template=pathlib.Path(template_name).stem,
        locale=locale)
    output_path = output_dir / output_name
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    with open(output_path, "w") as file_out:
        context_data  = {}
        context_data.update(defaults)
        context_data['items'] = items
        context_data.update(item)
        for part in template.generate(**context_data):
            file_out.write(part)
    return output_path


def layout2pdf(layout, layout_name, code, template_name, locale):
    """Generate .pdf files from layout sources."""
    layout = pathlib.Path(layout)
    output_dir = pathlib.Path("var", "pdf")
    output_path = output_dir / "{layout} - {code} - {locale} - {template}.pdf".format(
        code=code,
        layout=layout_name,
        template=pathlib.Path(template_name).stem,
        locale=locale)
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    if layout.suffix == ".svg":
        return inkscape2pdf(layout, output_path)
    elif layout.suffix == ".sla":
        return scribusexport(layout, output_path)


def inkscape2pdf(input_path, output_path):
    command = [
        "inkscape",
        "--without-gui",
        "--file={}".format(shlex.quote(str(input_path.resolve()))),
        "--export-area-page",
        "--export-dpi=300",
        "--export-text-to-path",
        "--export-pdf={}".format(shlex.quote(str(output_path.resolve()))),
    ]
    command = ' '.join(command)
    print(command)
    subprocess.call(command, shell=True)
    return output_path


def scribusexport(input_path, output_path):
    scribus_cmd = './scribus-1.5.5-linux-x86_64.AppImage'
    scribusexports_path = pathlib.Path('scribusexports.py')
    command = [
        scribus_cmd,
        "--no-splash",
        "--no-gui",
        "-py {}".format(shlex.quote(str(scribusexports_path.resolve()))),
        "--output-name={}".format(shlex.quote(str(output_path.resolve()))),
        "{}".format(shlex.quote(str(input_path.resolve()))),
        "--",
        ]
    command = ' '.join(command)
    print(command)
    subprocess.call(command, shell=True)
    return output_path


def layout2png(layout, layout_name, code, template_name, locale):
    """Generate .png files from SVG sources."""
    layout = pathlib.Path(layout)
    output_dir = pathlib.Path("var", "png")
    output_path = output_dir / "{layout} - {code} - {locale} - {template}.png".format(
        code=code,
        layout=layout_name,
        template=pathlib.Path(template_name).stem,
        locale=locale)
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    if layout.suffix == ".svg":
        return inkscape2png(layout, output_path)
    if layout.suffix == ".sla":
        return scribusexport(layout, output_path)
    

def inkscape2png(input_path, output_path):
    command = [
        "inkscape",
        "--without-gui",
        "--file={}".format(shlex.quote(str(input_path.resolve()))),
        "--export-area-page",
        "--export-dpi=300",
        "--export-text-to-path",
        "--export-png={}".format(shlex.quote(str(output_path.resolve()))),
    ]
    command = ' '.join(command)
    print(command)
    subprocess.call(command, shell=True)


def logo2png():
    output_dir = BUILD_DIR.resolve() / "img" / "logo"
    try:
        output_dir.mkdir(parents=True)
    except OSError:
        pass
    command = [
        "cp",
        str(IMAGE_DIR.resolve() / "logo" / "*.png"),
        str(output_dir),
    ]
    command = ' '.join(command)
    print(command)
    subprocess.call(command, shell=True)


def main():
    parser = argparse.ArgumentParser(description='Generate printable PDF files from sources.')
    parser.add_argument(
        '--locale',
        dest='locale',
        default='fr',
        type=str,
        choices=['fr'],
        help='Localization, e.g. language.',
    )
    parser.add_argument(
        '--bundle',
        dest='bundle',
        default="stages",
        type=str,
        help='A bundle is a list of items to process.',
    )
    parser.add_argument(
        '--layout',
        dest='layout',
        default="moo 55x84",
        type=str,
        help='Layout to use.',
    )
    parser.add_argument(
        '--id',
        dest='id',
        default=None,
        type=str,
        help='Limit process to item(s) matching pattern.',
    )
    parser.add_argument(
        '--template',
        dest='template',
        default=None,
        type=str,
        help='Limit process to template(s) matching pattern.',
    )
    parser.add_argument(
        '--version-name',
        dest='version',
        default=BUILD_VERSION,
        type=str,
        help="Build's version string.",
    )
    args = parser.parse_args()
    
    context_data = {}
    context_data['version'] = args.version

    print("### COMPILING TRANSLATIONS FOR LOCALE {}".format(args.locale))
    compile_locale(args.locale)

    print("### COPYING LOGO EXPORTS TO BUILD DIR")
    logo2png()

    print("### READING ITEMS IN BUNDLE {}".format(args.bundle))
    items = load_bundle(args.bundle, locale=args.locale)
    generated = OrderedDict()
    for item_id, item in items.items():
        if args.id and not fnmatch.fnmatchcase(item_id, args.id):
            continue
        print("")
        print("### PROCESSING \"{code}\"".format(code=item['id']))
        generated[item['id']] = []
        generated[item['id']].append(
            kra2png(code=item['id']))
        generated[item['id']].append(
            icon2pdf(code=item['id']))
        for template_name in item['templates']:
            if args.template and not fnmatch.fnmatchcase(template_name, args.template):
                continue
            layout_template = render_layout_template(item=item, items=items, layout_name=args.layout, template_name=template_name, locale=args.locale, defaults=context_data)
            generated[item['id']].append(layout_template)
            generated[item['id']].append(
                layout2pdf(layout=layout_template, layout_name=args.layout, code=item['id'], template_name=template_name, locale=args.locale))
            generated[item['id']].append(
                layout2png(layout=layout_template, layout_name=args.layout, code=item['id'], template_name=template_name, locale=args.locale))
        if args.id:
            break
    if not generated:
        if args.id:
            print("FOUND 0 ITEMS MATCHING GIVEN --id={}".format(args.id))
    print(str(generated))
    print("")
    print("### DONE :)")


if __name__ == "__main__":
    main()
